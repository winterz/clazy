# qt6-qlatin1char-to-u

Warns when using `QLatin1Char` and replaces it with `u`.
